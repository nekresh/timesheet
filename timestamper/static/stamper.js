// see http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
function dataURItoBlob(dataURI)
{
	// convert base64/URLEncoded data componenet to raw binary data held in a string
	var byteString = dataURI.split(',')[1];
	if (dataURI.split(',')[0].indexOf('base64') >= 0)
		byteString = atob(byteString);
	else
		byteString = unescape(byteString);

	// separate out the mime type component
	var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

	// write the bytes of the string to an ArrayBuffer
	var array = [];
	for (var i = 0; i < byteString.length; i++)
	{
		array.push(byteString.charCodeAt(i));
	}

	// write the ArrayBuffer to a blob, and you're done
	return new Blob([new Uint8Array(array)], {type: mimeString});
}

function captureWebcamImage(video, canvas)
{
	var rect = video.getBoundingClientRect();
	canvas.width = rect.width;
	canvas.height = rect.height;
	var context = canvas.getContext("2d");
	context.drawImage(video, 0, 0);
	var data = canvas.toDataURL("image/jpeg");
	return dataURItoBlob(data);
}

function initButtons(click)
{
	$("button[data-login]").each(function() {
		var login = $(this).data("login");
		$(this).text(login);
		$(this).click(function() {
			click(login);
		});
	});
}
