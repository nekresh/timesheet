from django.db import models

# Create your models here.
class Contract(models.Model):
    login = models.CharField(max_length=100)
    start_date = models.DateField()
    end_date = models.DateField()

    def __unicode__(self):
        return "Contract for %s" % self.login

class TimeStamp(models.Model):
    login = models.CharField(max_length=100)
    time = models.DateTimeField()
    photo = models.ImageField(upload_to="shots")

    def __unicode__(self):
        return "Timestamp for %s" % self.login
