from django.contrib import admin
from timestamper.forms import DateFilterForm
import datetime

class DateListFilter(admin.filters.FieldListFilter):
    template = 'date_filter/filter.html'

    def __init__(self, field, request, params, model, model_admin, field_path):
        self.lookup_kwarg = field_path
        super(DateListFilter, self).__init__(field, request, params, model, model_admin, field_path)
        self.form = self.get_form(request)
    
    def choices(self, cl):
        return []

    def expected_parameters(self):
        return [self.lookup_kwarg]

    def get_form(self, request):
        return DateFilterForm(data=self.used_parameters,
                field_name=self.field_path)

    def queryset(self, request, queryset):
        if self.form.is_valid():
            time = self.form.cleaned_data[self.field_path]
            if time:
                params = {
                    '%s__gte' % self.field_path: time,
                    '%s__lt' % self.field_path: time + datetime.timedelta(1)
                }
                return queryset.filter(**params)
        return queryset
