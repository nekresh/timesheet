from django import forms
from django.contrib.admin.widgets import AdminDateWidget

class DateFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        field_name = kwargs.pop('field_name')
        super(DateFilterForm, self).__init__(*args, **kwargs)

        self.fields[field_name] = forms.DateField(
            label='',
            widget=AdminDateWidget(
                attrs={'placeholder': 'Date'}
            ),
            localize=False,
            required=False
        )

class TimestampForm(forms.Form):
    login = forms.CharField(max_length=100)
    photo = forms.ImageField()
