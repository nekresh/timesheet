from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils import timezone
from timestamper.models import *
from timestamper.forms import TimestampForm
from timestamper.responses import JsonResponse
from datetime import date

@ensure_csrf_cookie
def index(request):
    today = date.today()

    if request.method == "POST":
        form = TimestampForm(request.POST, request.FILES)
        if form.is_valid():
            stamp = TimeStamp()
            stamp.login = form.cleaned_data["login"]
            stamp.time = timezone.now()
            stamp.photo = form.cleaned_data["photo"]
            stamp.save()
            return JsonResponse({ "url": stamp.photo.url })

    contracts = Contract.objects.filter(start_date__lte=today, end_date__gte=today)
    return render(request, "index.html", { "contracts": contracts })
