from django.contrib import admin
from django.contrib.admin import ModelAdmin
from timestamper.models import *
from timestamper.filters import DateListFilter

class ContractAdmin(ModelAdmin):
    list_display = ('login', 'start_date', 'end_date')
    search_fields = ['login']
    list_filter = ('login',)

class TimeStampAdmin(ModelAdmin):
    date_hierarchy = 'time'
    list_display = ('login', 'time', 'photo')
    search_fields = ['login']
    list_filter = ('login',)

class Stamp(TimeStamp):
        class Meta:
                proxy = True

class StampAdmin(TimeStampAdmin):
    def get_login_image(self, obj):
        return '<img src="http://cdn.local.epitech.eu/userprofil/profilview/%s.jpg" />' % obj.login
    get_login_image.allow_tags = True
    get_login_image.short_description = 'login image'
    get_login_image.admin_order_field = 'login'

    def get_photo(self, obj):
        return '<img src="%s" />' % obj.photo.url
    get_photo.allow_tags = True
    get_photo.short_description = 'photo'

    list_filter = (
        'login',
        ('time', DateListFilter),
    )
    list_display = ('login', 'get_login_image', 'time', 'get_photo')

class ContractByDate(Contract):
    class Meta:
        proxy = True

class ContractByDateAdmin(ModelAdmin):
    def get_urls(self):
        from django.conf.urls import patterns, url

        info = self.model._meta.app_label, self.model._meta.module_name

        urlpatterns = patterns('',
            url(r'^$', self.index, name='%s_%s_changelist' % info),
        )
        return urlpatterns

    def index(self, request):
        from datetime import date, datetime, timedelta
        from django.template.response import TemplateResponse
        from django.utils import timezone
        from django.db.models import Min, Max
        
        opts = self.model._meta
        app_label = opts.app_label
        filter_date = timezone.now().date()
        if request.method == 'POST':
            try:
                filter_date = datetime.strptime(request.POST['filter_date'], "%Y-%m-%d").date()
            except:
                pass
        contracts = Contract.objects.filter(start_date__lte=filter_date, end_date__gte=filter_date)
        logins = [c.login for c in contracts]
        timestamps = TimeStamp.objects.filter(login__in=logins, time__range=(filter_date, filter_date + timedelta(1))).values('login').annotate(time_min=Min('time'), time_max=Max('time'))
        stamp_dict = { s["login"]: s for s in timestamps}
        contracts_stamps = [ { 'contract': c, 'stamps': stamp_dict.get(c.login), 'style': self.get_style(c, stamp_dict.get(c.login)) } for c in contracts]
        return TemplateResponse(request, "admin/ContractByDate/index.html", { "contracts_stamps": contracts_stamps, "filter_date": filter_date, "app_label": app_label, "opts": opts }, current_app=self.admin_site.name)

    def get_style(self, contract, timestamps):
        from datetime import timedelta
        if timestamps:
            diff = timestamps["time_max"] - timestamps["time_min"]
            if diff > timedelta(hours=6):
                return "ok"
            else:
                return "middle"
        else:
            return "ko"

admin.site.register(Contract, ContractAdmin)
admin.site.register(TimeStamp, TimeStampAdmin)
admin.site.register(Stamp, StampAdmin)
admin.site.register(ContractByDate, ContractByDateAdmin)
